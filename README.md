# glmd

`glmd` (GitLab Markdown) is a
[CLI](https://en.wikipedia.org/wiki/Command-line_interface) to extract and
render [GitLab Flavoured
Markdown](https://docs.gitlab.com/ee/user/markdown.html) files from GitLab
projects. The files are converted to HTML by [GitLab's Markdown
API](https://docs.gitlab.com/ee/api/markdown.html).

## Download

Download the `glmd` binary from the GitLab [package registry](https://gitlab.com/samherrmann/glmd/-/packages/).


## Development

* Run `make lint` to run the linting tools (static code analysis).
* Run `make test` to execute all tests.
* Run `make test-skip-e2e` to execute all tests except e2e tests.
* Run `make run` to render the [example docs](testdata/examples.md). The
  rendered example docs are written to `out/docs`
* Run `make clean` to remove all untracked files.
* Run `make build` to build the executable.
