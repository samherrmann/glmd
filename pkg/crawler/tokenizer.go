package crawler

import (
	"fmt"
	"io"

	"golang.org/x/net/html"
)

// Tokenizer returns a stream of HTML tokens from its Token method.
type Tokenizer interface {
	// Token returns a stream of HTML tokens. Each call to Token returns the next
	// token in the stream. Token returns io.EOF when the end of the stream is
	// reached.
	Token() (*Token, error)
	io.Closer
}

// WriteTokens writes all tokens from the Tokenizer to the given Writer.
func WriteTokens(t Tokenizer, w io.Writer) error {
	for {
		token, err := t.Token()
		if err != nil {
			if err == io.EOF {
				return nil
			}
			return fmt.Errorf("reading html token: %w", err)
		}
		if _, err := w.Write([]byte(token.String())); err != nil {
			return fmt.Errorf("writing html token: %w", err)
		}
	}
}

func newTokenizer(stream io.ReadCloser) *tokenizer {
	return &tokenizer{
		stream:   stream,
		original: html.NewTokenizer(stream),
	}
}

type tokenizer struct {
	stream    io.ReadCloser
	original  *html.Tokenizer
	intercept func(token *Token) error
}

// Token implements the [Tokenizer] interface.
func (t *tokenizer) Token() (*Token, error) {
	tt := t.original
	tt.Next()
	err := tt.Err()
	if err != nil {
		return nil, err
	}
	token := &Token{htmlToken: ptr(tt.Token())}
	if t.intercept != nil {
		if err = t.intercept(token); err != nil {
			return nil, err
		}
	}
	return token, nil
}

// Token implements the [io.Closer] interface.
func (t *tokenizer) Close() error {
	return t.stream.Close()
}

// ptr returns the pointer to x.
func ptr[T any](x T) *T {
	return &x
}
