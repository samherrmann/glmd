// Package crawler provides the ability to iteratively fetch HTML documents by
// following the hyperlinks within each document.
package crawler

import (
	"fmt"
	"io"
	"net/url"
	"slices"

	"golang.org/x/net/html"
)

// New returns a Crawler that fetches the resource at the specified address
// using the given Fetcher.
func New(address string, fetcher Fetcher) *Crawler {
	return &Crawler{
		queue:   []string{address},
		fetcher: fetcher,
		visited: make(map[string]struct{}),
	}
}

type Crawler struct {
	queue   []string
	fetcher Fetcher
	visited map[string]struct{}
	// Skip is called for every hyperlink found. If Skip returns true, then
	// Fetcher is not called with the given address.
	Skip func(link *url.URL) bool
}

// Next fetches the next resource in the crawler's queue. If the queue is empty,
// then [io.EOF] is returned.
func (c *Crawler) Next() (*Result, error) {
	if len(c.queue) == 0 {
		return nil, io.EOF
	}

	address := c.queue[0]
	c.queue = slices.Delete(c.queue, 0, 1)

	response, err := c.fetcher.Fetch(address)
	if err != nil {
		return nil, err
	}

	sourceURL, err := url.Parse(address)
	if err != nil {
		return nil, fmt.Errorf("parsing source URL: %w", err)
	}

	tokenizer := newTokenizer(response)
	tokenizer.intercept = func(token *Token) error {
		if token.Type == html.StartTagToken {
			if token.Data == "a" {
				for _, attr := range token.Attr {
					if attr.Key == "href" {
						absURL, err := sourceURL.Parse(attr.Val)
						if err != nil {
							return fmt.Errorf("parsing hyperlink URL: %w", err)
						}

						token.Link = &Link{
							SourceURL: sourceURL,
							TargetURL: absURL,
						}

						if c.Skip != nil && c.Skip(token.Link.TargetURL) {
							token.Link.Skipped = true
							return nil
						}

						targetURL := cloneURL(token.Link.TargetURL)
						targetURL.Fragment = ""
						address := targetURL.String()

						_, visited := c.visited[address]
						if visited {
							return nil
						}

						inQueue := slices.Contains(c.queue, address)
						if inQueue {
							return nil
						}

						c.queue = append(c.queue, address)
						return nil
					}
				}
			}
		}
		return nil
	}

	c.visited[address] = struct{}{}
	return &Result{
		URL:  sourceURL,
		Body: tokenizer,
	}, nil
}

// Crawl calls the Next method until it returns io.EOF or another error and
// passes each [Result] to the given [Handler].
func (c *Crawler) Crawl(handler Handler) error {
	for {
		err := func() error {
			res, err := c.Next()
			if err != nil {
				return err
			}
			return handler.Handle(res)
		}()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
	}
}

// cloneURL returns a clone of u.
//
// References:
//   - https://github.com/golang/go/issues/38351
//   - https://github.com/golang/go/blob/go1.19/src/net/http/clone.go#L22
func cloneURL(u *url.URL) *url.URL {
	if u == nil {
		return nil
	}
	u2 := new(url.URL)
	*u2 = *u
	if u.User != nil {
		u2.User = new(url.Userinfo)
		*u2.User = *u.User
	}
	return u2
}
