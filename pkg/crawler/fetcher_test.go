package crawler

import (
	"bytes"
	"io"
	"testing"
	"testing/fstest"
)

func TestFSFetcher_Fetch(t *testing.T) {

	fs := fstest.MapFS{
		"foo": &fstest.MapFile{Data: []byte("foobar")},
	}

	fetcher := &FSFetcher{fs: fs}
	resp, err := fetcher.Fetch("foo")
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Close()

	var buf bytes.Buffer
	if _, err := io.Copy(&buf, resp); err != nil {
		t.Fatal(err)
	}

	got := buf.String()
	want := "foobar"

	if got != want {
		t.Errorf("got %v, want %v", got, want)
	}

}
