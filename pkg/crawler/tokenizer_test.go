package crawler

import (
	"io"
	"strings"
	"testing"
)

func Test_tokenizer_Token(t *testing.T) {

	t.Run("simple html", func(t *testing.T) {
		reader := strings.NewReader("<h1>foo</h1>")
		tr := newTokenizer(io.NopCloser(reader))

		assertTokenData(t, tr, "h1")
		assertTokenData(t, tr, "foo")
		assertTokenData(t, tr, "h1")
		assertTokenizerError(t, tr, io.EOF)
	})

	t.Run("calls intercept function for each token", func(t *testing.T) {
		reader := strings.NewReader("<h1>foo</h1>")
		tr := newTokenizer(io.NopCloser(reader))

		callCount := 0
		tr.intercept = func(token *Token) error {
			callCount++
			return nil
		}

		for {
			_, err := tr.Token()
			if err == io.EOF {
				break
			}
		}

		want := 3
		got := callCount
		if got != want {
			t.Fatalf("got %v, want %v", got, want)
		}
	})

	t.Run("intercept function can attach link to token", func(t *testing.T) {
		reader := strings.NewReader("foo")
		tr := newTokenizer(io.NopCloser(reader))

		link := &Link{}

		tr.intercept = func(token *Token) error {
			token.Link = link
			return nil
		}

		token, err := tr.Token()
		if err != nil {
			t.Fatal(err)
		}

		want := link
		got := token.Link
		if got != want {
			t.Fatalf("got %v, want %v", got, want)
		}
	})
}

func assertTokenData(t *testing.T, tokenizer Tokenizer, want string) {
	token, err := tokenizer.Token()
	if err != nil {
		t.Fatal(err)
	}
	got := token.Data
	if got != want {
		t.Fatalf("got %q, want %q", got, want)
	}
}

func assertTokenizerError(t *testing.T, tokenizer Tokenizer, want error) {
	_, got := tokenizer.Token()
	if got != want {
		t.Fatalf("got %q, want %q", got, want)
	}
}
