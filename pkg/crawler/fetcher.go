package crawler

import (
	"io"
	"io/fs"
	"net/http"
)

// Fetcher fetches the resource at a given URL.
type Fetcher interface {
	Fetch(address string) (io.ReadCloser, error)
}

// NewNetFetcher returns a NetFetcher.
func NewNetFetcher(c *http.Client) *NetFetcher {
	if c == nil {
		panic("nil http.Client")
	}
	return &NetFetcher{client: c}
}

// NetFetcher implements the Fetcher interface for an [net/http.Client]
type NetFetcher struct {
	client *http.Client
}

func (f *NetFetcher) Fetch(address string) (io.ReadCloser, error) {
	res, err := f.client.Get(address)
	if err != nil {
		return nil, err
	}
	return res.Body, HTTPStatusToError(res)
}

// NewFSFetcher returns a FSFetcher.
func NewFSFetcher(fs fs.FS) *FSFetcher {
	if fs == nil {
		panic("nil fs.FS")
	}
	return &FSFetcher{fs: fs}
}

// FSFetcher implements the Fetcher interface for an [io/fs.FS].
type FSFetcher struct {
	fs fs.FS
}

func (f *FSFetcher) Fetch(address string) (io.ReadCloser, error) {
	return f.fs.Open(address)
}
