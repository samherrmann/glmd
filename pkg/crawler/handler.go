package crawler

// Handler handles a [Result] returned by the [Crawler].
type Handler interface {
	Handle(res *Result) error
}
