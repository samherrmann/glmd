package crawler_test

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"gitlab.com/samherrmann/glmd/pkg/crawler"
)

func TestCrawler_Next(t *testing.T) {

	t.Run("first document not found", func(t *testing.T) {
		fetcher := &fetcherStub{}
		c := crawler.New("https://foo", fetcher)
		assertNotFound(t, c)
	})

	t.Run("first document without hyperlinks", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `foo`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		assertSuccess(t, c, "https://foo", "foo")
		assertEOF(t, c)
	})

	t.Run("first document with broken hyperlink", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `<a href="bar"></a>`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		assertSuccess(t, c, "https://foo", `<a href="bar"></a>`)
		assertNotFound(t, c)
	})

	t.Run("second document without hyperlink", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `<a href="https://bar"></a>`,
				"https://bar": `bar`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		assertSuccess(t, c, "https://foo", `<a href="https://bar"></a>`)
		assertSuccess(t, c, "https://bar", `bar`)
		assertEOF(t, c)
	})

	t.Run("second document with hyperlink", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `<a href="https://bar"></a>`,
				"https://bar": `<a href="https://baz"></a>`,
				"https://baz": `baz`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		assertSuccess(t, c, "https://foo", `<a href="https://bar"></a>`)
		assertSuccess(t, c, "https://bar", `<a href="https://baz"></a>`)
		assertSuccess(t, c, "https://baz", `baz`)
		assertEOF(t, c)
	})

	t.Run("document with multiple hyperlinks", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `<a href="https://bar"></a><a href="https://baz"></a>`,
				"https://bar": `bar`,
				"https://baz": `baz`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		assertSuccess(t, c, "https://foo", `<a href="https://bar"></a><a href="https://baz"></a>`)
		assertSuccess(t, c, "https://bar", `bar`)
		assertSuccess(t, c, "https://baz", `baz`)
		assertEOF(t, c)
	})

	t.Run("document with multiple instances of same hyperlink", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `<a href="https://bar"></a><a href="https://bar"></a>`,
				"https://bar": `bar`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		assertSuccess(t, c, "https://foo", `<a href="https://bar"></a><a href="https://bar"></a>`)
		assertSuccess(t, c, "https://bar", `bar`)
		assertEOF(t, c)
	})

	t.Run("success after error", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `<a href="https://bar"></a><a href="https://baz"></a>`,
				"https://baz": `baz`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		assertSuccess(t, c, "https://foo", `<a href="https://bar"></a><a href="https://baz"></a>`)
		assertNotFound(t, c)
		assertSuccess(t, c, "https://baz", `baz`)
		assertEOF(t, c)
	})

	t.Run("circular hyperlinks", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `<a href="https://bar"></a>`,
				"https://bar": `<a href="https://foo"></a>`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		assertSuccess(t, c, "https://foo", `<a href="https://bar"></a>`)
		assertSuccess(t, c, "https://bar", `<a href="https://foo"></a>`)
		assertEOF(t, c)
	})

	t.Run("indirect circular hyperlinks", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `<a href="https://bar"></a>`,
				"https://bar": `<a href="https://baz"></a>`,
				"https://baz": `<a href="https://foo"></a>`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		assertSuccess(t, c, "https://foo", `<a href="https://bar"></a>`)
		assertSuccess(t, c, "https://bar", `<a href="https://baz"></a>`)
		assertSuccess(t, c, "https://baz", `<a href="https://foo"></a>`)
		assertEOF(t, c)
	})

	t.Run("skip specific URL", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://foo": `<a href="https://bar"></a><a href="https://baz"></a>`,
				"https://bar": `bar`,
				"https://baz": `baz`,
			},
		}
		c := crawler.New("https://foo", fetcher)
		c.Skip = func(link *url.URL) bool { return link.String() == "https://baz" }
		assertSuccess(t, c, "https://foo", `<a href="https://bar"></a><a href="https://baz"></a>`)
		assertSuccess(t, c, "https://bar", `bar`)
		assertEOF(t, c)
	})

	t.Run("resolve relative links", func(t *testing.T) {
		fetcher := &fetcherStub{
			pages: map[string]string{
				"https://example.com/segment/foo": `<a href="bar"></a><a href="../baz"></a>`,
				"https://example.com/segment/bar": `bar`,
				"https://example.com/baz":         `baz`,
			},
		}
		c := crawler.New("https://example.com/segment/foo", fetcher)
		assertSuccess(t, c, "https://example.com/segment/foo", `<a href="bar"></a><a href="../baz"></a>`)
		assertSuccess(t, c, "https://example.com/segment/bar", `bar`)
		assertSuccess(t, c, "https://example.com/baz", `baz`)
		assertEOF(t, c)
	})

	t.Run("assigns Link to Token", func(t *testing.T) {

		tests := []struct {
			name           string
			ref            string
			wantSourceAddr string
			wantTargetAddr string
			skip           bool
		}{
			{
				name:           "relative url",
				ref:            "bar",
				wantSourceAddr: "https://foo",
				wantTargetAddr: "https://foo/bar",
			},
			{
				name:           "absolute url",
				ref:            "https://bar",
				wantSourceAddr: "https://foo",
				wantTargetAddr: "https://bar",
			},
			{
				name:           "skip",
				ref:            "https://bar",
				wantSourceAddr: "https://foo",
				wantTargetAddr: "https://bar",
				skip:           true,
			},
		}

		for _, tt := range tests {
			fetcher := &fetcherStub{
				pages: map[string]string{
					"https://foo": fmt.Sprintf(`<a href="%v">`, tt.ref),
				},
			}
			c := crawler.New("https://foo", fetcher)
			c.Skip = func(link *url.URL) bool { return tt.skip }

			result, err := c.Next()
			if err != nil {
				t.Fatal(err)
			}

			token, err := result.Body.Token()
			if err != nil {
				t.Fatal(err)
			}

			{
				got := token.Link.SourceURL.String()
				want := tt.wantSourceAddr
				if got != want {
					t.Fatalf("got %v, want %v", got, want)
				}
			}

			{
				got := token.Link.TargetURL.String()
				want := tt.wantTargetAddr
				if got != want {
					t.Fatalf("got %v, want %v", got, want)
				}
			}

			{
				got := token.Link.Skipped
				want := tt.skip
				if got != want {
					t.Fatalf("got %v, want %v", got, want)
				}
			}
		}

	})
}

type fetcherStub struct {
	pages map[string]string
}

func (f *fetcherStub) Fetch(address string) (io.ReadCloser, error) {
	page, exists := f.pages[address]
	if !exists {
		return nil, &crawler.HTTPError{Address: address, Code: http.StatusNotFound}
	}
	return newStringReadCloser(page), nil
}

func newStringReadCloser(s string) io.ReadCloser {
	r := strings.NewReader(s)
	return io.NopCloser(r)
}

func assertSuccess(t *testing.T, c *crawler.Crawler, wantURL string, wantHTML string) {
	res, err := c.Next()
	if err != nil {
		t.Fatal(err)
	}

	{
		got := res.URL.String()
		if got != wantURL {
			t.Fatalf("got %v, want %v", got, wantURL)
		}
	}

	var buf bytes.Buffer
	body := res.Body
	defer body.Close()
	if err := crawler.WriteTokens(body, &buf); err != nil {
		t.Fatal(err)
	}

	got := buf.String()
	if got != wantHTML {
		t.Fatalf("got %v, want %v", got, wantHTML)
	}
}

func assertEOF(t *testing.T, c *crawler.Crawler) {
	_, err := c.Next()
	got := err
	want := io.EOF
	if got != want {
		t.Fatalf("got %v, want %v", got, want)
	}
}

func assertNotFound(t *testing.T, c *crawler.Crawler) {
	_, err := c.Next()
	var httpErr *crawler.HTTPError
	if !errors.As(err, &httpErr) {
		t.Fatal(err)
	}
	got := httpErr.Code
	want := http.StatusNotFound
	if got != want {
		t.Fatalf("got %v, want %v", got, want)
	}
}
