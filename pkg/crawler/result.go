package crawler

import "net/url"

// Result is a result returned by the [Crawler].
type Result struct {
	URL  *url.URL
	Body Tokenizer
}
