package crawler_test

import (
	"fmt"
	"io"
	"net/http"
	"os"

	"gitlab.com/samherrmann/glmd/pkg/crawler"
)

func Example() {
	if err := app(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func app() error {
	address := "https://example.com"

	fetcher := crawler.NewNetFetcher(http.DefaultClient)
	c := crawler.New(address, fetcher)

	for {
		res, err := c.Next()
		if err != nil {
			if err == io.EOF {
				return nil
			}
			return err
		}
		body := res.Body
		for {
			token, err := body.Token()
			if err != nil {
				if err == io.EOF {
					return nil
				}
				return err
			}
			fmt.Print(token)
		}
	}
}
