package crawler

import (
	"net/url"

	"golang.org/x/net/html"
)

type htmlToken = html.Token

type Token struct {
	*htmlToken
	// Link contains hyperlink information for anchor start tag Tokens, i.e. <a>.
	// Link is nil for all other Tokens.
	Link *Link
}

// Link contains hyperlink information.
type Link struct {
	// SourceURL is the URL of the document that contains the hyperlink.
	SourceURL *url.URL
	// TargetURL is the URL of the hyperlink destination.
	TargetURL *url.URL
	// Skipped if true, indicates that the crawler did not follow this link.
	Skipped bool
}
