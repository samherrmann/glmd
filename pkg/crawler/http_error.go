package crawler

import (
	"errors"
	"fmt"
	"io"
	"net/http"
)

// HTTPStatusToError returns an error based on the status code of the given
// response. If the status code is not within the range of 400 to 599 inclusive,
// then nil is returned.
func HTTPStatusToError(res *http.Response) error {
	if res.StatusCode >= 400 && res.StatusCode <= 599 {
		httpStatusErr := &HTTPError{Address: res.Request.URL.String(), Code: res.StatusCode}
		body, err := io.ReadAll(res.Body)
		if err != nil {
			return errors.Join(httpStatusErr, err)
		}
		httpStatusErr.Details = string(body)
		return httpStatusErr
	}
	return nil
}

type HTTPError struct {
	Code    int
	Address string
	Details string
}

func (e *HTTPError) Error() string {
	status := http.StatusText(e.Code)
	msg := fmt.Sprintf("%v: http code %v: %v", e.Address, e.Code, status)
	if e.Details != "" {
		msg = fmt.Sprintf("%v: %v", msg, e.Details)
	}
	return msg
}
