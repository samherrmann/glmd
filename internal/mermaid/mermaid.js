import mermaid from 'mermaid';

mermaid.initialize({ startOnLoad: true });

const els = document.querySelectorAll('pre.language-mermaid')
for (const [index, el] of els.entries()) {
  mermaid.render(`mermaid-${index}`, el.textContent).then(result => {
    el.parentElement.innerHTML = result.svg;
  });
}
