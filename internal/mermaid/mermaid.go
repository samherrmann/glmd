// Package mermaid provides the ability to write the embedded Mermaid library to
// a filesystem directory and generate HTML script elements to use it in a web
// page.
package mermaid

import (
	"embed"
	"io"
	"os"
	"path/filepath"

	"golang.org/x/net/html"
)

//go:embed mermaid.bundle.js
var embeddedFS embed.FS

const bundleFilename = "mermaid.bundle.js"

// WriteFile writes the embedded Mermaid bundle file to the given directory.
func WriteFile(dir string) error {
	if err := os.MkdirAll(dir, 0770); err != nil {
		return err
	}

	srcFile, err := embeddedFS.Open(bundleFilename)
	if err != nil {
		return err
	}
	defer srcFile.Close()

	dstFile, err := os.Create(filepath.Join(dir, bundleFilename))
	if err != nil {
		return err
	}
	defer dstFile.Close()

	_, err = io.Copy(dstFile, srcFile)
	return err
}

// NewHTMLNode return an HTML <script> element for the Mermaid bundle. The
// element's src attribute is set to a path relative to the given filename.
func NewHTMLNode(filename string) (*html.Node, error) {
	relBundleFilename, err := filepath.Rel(filepath.Dir(filename), bundleFilename)
	if err != nil {
		return nil, err
	}

	bundleNode := &html.Node{
		Data: "script",
		Type: html.ElementNode,
		Attr: []html.Attribute{
			{Key: "src", Val: relBundleFilename},
		},
	}

	return bundleNode, nil
}
