// Package stylesheets provides the ability to write embedded CSS files to a
// filesystem directory and link them into a HTML file.
package stylesheets

import (
	"embed"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"

	"golang.org/x/net/html"
)

//go:embed main.css syntax-highlight.css
var embeddedFS embed.FS

var filenames = []string{
	"main.css",
	"syntax-highlight.css",
}

// WriteFile writes the embedded stylesheet to the given directory.
func WriteFile(dir string) error {
	if err := os.MkdirAll(dir, 0770); err != nil {
		return err
	}

	for _, filename := range filenames {
		stylesFile, err := embeddedFS.Open(filename)
		if err != nil {
			return err
		}

		if err := copy(stylesFile, filepath.Join(dir, filename)); err != nil {
			return fmt.Errorf("cannot write %q in output directory %q: %w", filename, dir, err)
		}
	}

	return nil
}

// NewHTMLNodes returns a <link> element node for the given HTML filename that
// imports the stylesheet. The element's href attribute is set to a path
// relative to the given filename. The returned element node is usually appended
// to the <head> element.
func NewHTMLNodes(filename string) ([]*html.Node, error) {

	nodes := []*html.Node{}

	for _, stylesheetFilename := range filenames {
		relPath, err := filepath.Rel(filepath.Dir(filename), stylesheetFilename)
		if err != nil {
			return nil, err
		}
		node := &html.Node{
			Data: "link",
			Type: html.ElementNode,
			Attr: []html.Attribute{
				{Key: "rel", Val: "stylesheet"},
				{Key: "type", Val: "text/css"},
				{Key: "href", Val: relPath},
			},
		}
		nodes = append(nodes, node)
	}
	return nodes, nil
}

func copy(src fs.File, dstPath string) error {
	_, err := src.Stat()
	if err != nil {
		return err
	}
	out, err := os.Create(dstPath)
	if err != nil {
		return err
	}
	defer out.Close()

	if _, err = io.Copy(out, src); err != nil {
		return err
	}
	return out.Sync()
}
