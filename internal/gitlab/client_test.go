package gitlab_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/samherrmann/glmd/internal/gitlab"
)

func TestClient_RawFile(t *testing.T) {
	type testCase struct {
		description            string
		token                  string
		wantNoAccessTokenError bool
	}

	testCases := []testCase{
		{
			description:            "should include NoAccessTokenError",
			token:                  "",
			wantNoAccessTokenError: true,
		},
		{
			description:            "should not include NoAccessTokenError",
			token:                  "a1b2c3",
			wantNoAccessTokenError: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(http.StatusNotFound)
				w.Write([]byte{})
			}))

			c := gitlab.NewClient(server.URL, tc.token)
			_, err := c.RawFile("/my-group/my-project", "entrypoint-file.ext", "my-branch")
			noAccessTokenErr := &gitlab.NoAccessTokenError{}
			if tc.wantNoAccessTokenError != errors.As(err, &noAccessTokenErr) {
				t.Fatal(err)
			}
		})
	}
}
