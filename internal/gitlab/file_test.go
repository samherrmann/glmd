package gitlab_test

import (
	"testing"

	"gitlab.com/samherrmann/glmd/internal/gitlab"
)

func TestFileInfo_Dest(t *testing.T) {
	type args struct {
		entrypoint string
		location   string
	}
	type want struct {
		dest string
	}
	tests := []struct {
		description string
		args        *args
		want        *want
		wantErr     bool
	}{
		{
			description: "should return destination for entrypoint file",
			args: &args{
				location: "my-file.ext",
			},
			want: &want{
				dest: "my-file.ext",
			},
		},
		{
			description: "should return destination for relative path",
			args: &args{
				entrypoint: "entrypoint.ext",
				location:   "path/to/my-file.ext",
			},
			want: &want{
				dest: "my-group1/my-project1/my-branch1/path/to/my-file.ext",
			},
		},
		{
			description: "should return destination for absolute URL of entrypoint project",
			args: &args{
				entrypoint: "entrypoint.ext",
				location:   "https://gitlab.com/my-group1/my-project1/-/blob/my-branch1/path/to/my-file.ext",
			},
			want: &want{
				dest: "my-group1/my-project1/my-branch1/path/to/my-file.ext",
			},
		},
		{
			description: "should return destination for absolute URL of another project",
			args: &args{
				entrypoint: "entrypoint.ext",
				location:   "https://gitlab.com/my-group2/my-project2/-/blob/my-branch2/path/to/my-file.ext",
			},
			want: &want{
				dest: "my-group2/my-project2/my-branch2/path/to/my-file.ext",
			},
		},
	}
	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			remoteURL := "https://gitlab.com/my-group1/my-project1.git"
			gitRef := "my-branch1"

			var fileInfo *gitlab.FileInfo
			if tc.args.entrypoint != "" {
				entrypointFileInfo, err := gitlab.Stat(remoteURL, gitRef, tc.args.entrypoint)
				if (err != nil) != tc.wantErr {
					t.Fatalf("unexpected error: %v", err)
				}
				fileInfo, err = entrypointFileInfo.Stat(tc.args.location)
				if (err != nil) != tc.wantErr {
					t.Fatalf("unexpected error: %v", err)
				}
			} else {
				var err error
				fileInfo, err = gitlab.Stat(remoteURL, gitRef, tc.args.location)
				if (err != nil) != tc.wantErr {
					t.Fatalf("unexpected error: %v", err)
				}
			}
			got := fileInfo.Dest()
			want := tc.want.dest
			if got != want {
				t.Fatalf("got %v, want %v", got, want)
			}
		})
	}
}

func TestFileInfo_Src(t *testing.T) {
	type args struct {
		entrypoint string
		location   string
	}
	type want struct {
		src string
	}
	tests := []struct {
		description string
		args        *args
		want        *want
		wantErr     bool
	}{
		{
			description: "should return path for entrypoint file",
			args: &args{
				location: "entrypoint.ext",
			},
			want: &want{
				src: "entrypoint.ext",
			},
		},
		{
			description: "should return path for file in entrypoint project linked by relative path",
			args: &args{
				entrypoint: "entrypoint.ext",
				location:   "a/b/c/my-file.ext",
			},
			want: &want{
				src: "a/b/c/my-file.ext",
			},
		},
		{
			description: "should return path for file in entrypoint project (same git ref) linked by absolute URL",
			args: &args{
				entrypoint: "entrypoint.ext",
				location:   "https://gitlab.com/my-group1/my-project1/-/blob/my-branch1/a/b/c/my-file.ext",
			},
			want: &want{
				src: "a/b/c/my-file.ext",
			},
		},
		{
			description: "should return path for file in entrypoint project (different Git ref) linked by absolute URL",
			args: &args{
				entrypoint: "entrypoint.ext",
				location:   "https://gitlab.com/my-group1/my-project1/-/blob/my-branch2/a/b/c/my-file.ext",
			},
			want: &want{
				src: "https://gitlab.com/my-group1/my-project1/-/blob/my-branch2/a/b/c/my-file.ext",
			},
		},
		{
			description: "should return path for file in different project linked by absolute URL",
			args: &args{
				entrypoint: "entrypoint.ext",
				location:   "https://gitlab.com/my-group2/my-project2/-/blob/my-branch2/a/b/c/my-file.ext",
			},
			want: &want{
				src: "https://gitlab.com/my-group2/my-project2/-/blob/my-branch2/a/b/c/my-file.ext",
			},
		},
	}
	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			remoteURL := "https://gitlab.com/my-group1/my-project1.git"
			gitRef := "my-branch1"

			var fileInfo *gitlab.FileInfo
			if tc.args.entrypoint != "" {
				entrypointFileInfo, err := gitlab.Stat(remoteURL, gitRef, tc.args.entrypoint)
				if (err != nil) != tc.wantErr {
					t.Fatalf("unexpected error: %v", err)
				}
				fileInfo, err = entrypointFileInfo.Stat(tc.args.location)
				if (err != nil) != tc.wantErr {
					t.Fatalf("unexpected error: %v", err)
				}
			} else {
				var err error
				fileInfo, err = gitlab.Stat(remoteURL, gitRef, tc.args.location)
				if (err != nil) != tc.wantErr {
					t.Fatalf("unexpected error: %v", err)
				}
			}
			got := fileInfo.Src().String()
			want := tc.want.src
			if got != want {
				t.Fatalf("got %v, want %v", got, want)
			}
		})
	}
}
