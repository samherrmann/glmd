package gitlab

import (
	"bytes"
	"fmt"
	"io"
	"strings"
)

// NewFetcher returns a new [Fetcher].
func NewFetcher(client *Client) *Fetcher {
	if client == nil {
		panic("nil Client")
	}
	return &Fetcher{client: client}
}

// Fetcher implements the [crawler.Fetcher] interface for files hosted on
// GitLab.
type Fetcher struct {
	client *Client
}

// Fetch returns the contents of the file at the given GitLab address in HTML
// format. The address is expected to be the URL as seen in the web browser when
// navigating to the file in GitLab.
func (f *Fetcher) Fetch(address string) (io.ReadCloser, error) {
	url, err := ParseURL(address)
	if err != nil {
		return nil, err
	}

	if url.ObjectType() != ObjectTypeBlob {
		return nil, fmt.Errorf("%v objects are not yet supported", url.ObjectType())
	}

	file, err := f.client.RawFile(
		url.ProjectPath(),
		url.ObjectPath(),
		url.GitRef(),
	)
	if err != nil {
		return nil, fmt.Errorf("fetching raw file contents for %q: %w", address, err)
	}
	defer file.Close()

	var buf bytes.Buffer
	if _, err := io.Copy(&buf, file); err != nil {
		return nil, fmt.Errorf("copying file contents: %w", err)
	}

	md, err := f.client.MarkdownToHTML(buf.String())
	if err != nil {
		return nil, fmt.Errorf("converting markdown to html: %w", err)
	}

	return io.NopCloser(strings.NewReader(md)), nil
}
