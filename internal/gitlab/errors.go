package gitlab

import (
	"fmt"
	"net/url"
)

type RequestError struct {
	status string
	url    *url.URL
	err    error
}

func (err *RequestError) Error() string {
	msg := fmt.Sprintf("%v: %v", err.status, err.url)
	if err.err != nil {
		msg = fmt.Sprintf("%v: %v", msg, err.err.Error())
	}
	return msg
}

func (err *RequestError) Unwrap() error {
	return err.err
}

type NoAccessTokenError struct{}

func (err *NoAccessTokenError) Error() string {
	return "no access token was provided, is this a private repository?"
}
