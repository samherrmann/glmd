package gitlab

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	pathpkg "path"
	"strings"
)

// Client is a GitLab client.
type Client struct {
	serverURL string
	token     string
}

// NewClient returns a new GitLab client.
func NewClient(serverURL string, token string) *Client {
	return &Client{serverURL, token}
}

// ServerURL returns the URL of the GitLab server.
func (c *Client) ServerURL() string {
	return c.serverURL
}

// MarkdownToHTML returns the HTML of the given Markdown string. This function
// uses GitLab's markdown API, using the GitLab instance provided by serverURL.
//
// https://docs.gitlab.com/ee/api/markdown.html
func (c *Client) MarkdownToHTML(markdown string) (string, error) {
	// TODO: Does it make sense to convert the signature of this method to accept
	// and return an io.Reader instead of a string?
	reqBody := strings.NewReader(fmt.Sprintf(`{"text":%q,"gfm":true}`, markdown))
	res, err := c.post("/markdown", &options{body: reqBody})
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusCreated {
		return "", errors.New(res.Status)
	}

	body := &markdownResponse{}
	if err := json.NewDecoder(res.Body).Decode(body); err != nil {
		return "", err
	}
	return body.HTML, nil
}

// markdownResponse is the response from the GitLab markdown API.
//
// https://docs.gitlab.com/ee/api/markdown.html
type markdownResponse struct {
	HTML string `json:"html"`
}

// RawFile returns the raw file content.
//
// https://docs.gitlab.com/ee/api/repository_files.html#get-raw-file-from-repository
func (c *Client) RawFile(projectID string, filename string, gitRef string) (io.ReadCloser, error) {
	apiPath := pathpkg.Join(
		"/projects",
		url.PathEscape(projectID),
		"repository/files",
		url.PathEscape(filename),
		"raw",
	)

	query := url.Values{}
	query.Add("ref", gitRef)

	res, err := c.get(apiPath, &options{query: query})
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		err := &RequestError{status: res.Status, url: res.Request.URL}
		if res.StatusCode == http.StatusNotFound && c.token == "" {
			// GitLab returns 404 instead of 401 when requesting a file from a
			// private repository without an access token.
			// https://gitlab.com/gitlab-org/gitlab/-/issues/334897
			err.err = &NoAccessTokenError{}
		}
		return nil, err
	}
	return res.Body, nil
}

// Tree returns a list of repository files.
//
// https://docs.gitlab.com/ee/api/repositories.html
func (c *Client) Tree(projectID string, path string, gitRef string) (string, error) {
	apiPath := pathpkg.Join(
		"/projects",
		url.PathEscape(projectID),
		"repository/tree",
	)

	query := url.Values{}
	query.Add("path", path)
	query.Add("ref", gitRef)

	res, err := c.get(apiPath, &options{query: query})
	if err != nil {
		return "", err
	}
	buf := bytes.NewBuffer([]byte{})
	if _, err := buf.ReadFrom(res.Body); err != nil {
		return "", err
	}
	return buf.String(), nil
}

// get sends an HTTP GET request to the GitLab server and returns an HTTP
// response.
func (c *Client) get(path string, opts *options) (*http.Response, error) {
	return c.fetch("GET", path, opts)
}

// post sends an HTTP POST request to the GitLab server and returns an HTTP
// response.
func (c *Client) post(path string, opts *options) (*http.Response, error) {
	return c.fetch("POST", path, opts)
}

// fetch sends an HTTP request to the GitLab server and returns an HTTP
// response.
func (c *Client) fetch(method string, path string, opts *options) (*http.Response, error) {
	if opts == nil {
		opts = &options{}
	}
	if !strings.HasPrefix(path, "/") {
		path = "/" + path
	}
	apiURL := c.serverURL + "/api/v4" + path + "?" + opts.query.Encode()
	req, err := http.NewRequest(method, apiURL, opts.body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")
	if c.token != "" {
		req.Header.Add("PRIVATE-TOKEN", c.token)
	}
	client := &http.Client{}
	return client.Do(req)
}

type options struct {
	query url.Values
	body  io.Reader
}
