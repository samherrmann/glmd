package gitlab

import (
	"io"
	"os"
	"path"
	"path/filepath"
)

// FileInfo describes a file tracked in Git/GitLab.
type FileInfo struct {
	src  *URL
	dest string
}

// NewFileInfo returns a new FileInfo.
func NewFileInfo(src *URL, dest string) *FileInfo {
	return &FileInfo{src, dest}
}

// Stat returns a parsed GitLab FileInfo structure for the given location.
// location is expected to be a local path or the URL as seen in the web browser
// when navigating to the file in GitLab.
func Stat(remoteURL string, gitRef string, filename string) (*FileInfo, error) {
	srcURL, err := ParseBlobURL(remoteURL, gitRef, filename)
	if err != nil {
		return nil, err
	}
	return NewFileInfo(srcURL, filepath.Base(filename)), nil
}

// IsRemote returns true if the file source is not located in the same project
// and branch/tag as the entrypoint file.
func (f *FileInfo) IsRemote() bool {
	return f.src.IsRemote()
}

// Src returns the file's source location.
func (f *FileInfo) Src() *URL {
	return f.src
}

// Dest returns the file's destination path.
func (f *FileInfo) Dest() string {
	return f.dest
}

// RelDestPath returns the destination path of receiver f relative to the
// destination path of the given base file..
func (f *FileInfo) RelDestPath(base *FileInfo) (string, error) {
	basePath := path.Dir(base.Dest())
	targetPath := f.Dest()
	return filepath.Rel(basePath, targetPath)
}

// Contents returns the contents of the file from the GitLab server.
func (f *FileInfo) Contents(token string) (io.ReadCloser, error) {
	if f.IsRemote() {
		c := NewClient(f.src.Origin(), token)
		return c.RawFile(
			f.src.ProjectPath(),
			f.src.ObjectPath(),
			f.src.GitRef(),
		)
	}
	return os.Open(f.Src().String())
}

// Stat returns a parsed GitLab FileInfo structure for the given location
// relative to the receiver FileInfo.
func (f *FileInfo) Stat(location string) (*FileInfo, error) {
	u, err := f.src.Parse(location)
	if err != nil {
		return nil, err
	}
	return &FileInfo{
		src:  u,
		dest: filepath.Join(u.ProjectPath(), u.GitRef(), u.ObjectPath()),
	}, nil
}
