package gitlab_test

import (
	"testing"

	"gitlab.com/samherrmann/glmd/internal/gitlab"
)

func TestURL_Parse(t *testing.T) {
	type args struct {
		url string
	}
	type want struct {
		projectPath string
		objectPath  string
		gitRef      string
		objectType  gitlab.ObjectType
		url         string
	}
	tests := []struct {
		description string
		args        *args
		want        *want
		wantErr     bool
	}{
		{
			description: "should parse absolute URL to project root",
			args: &args{
				url: "https://gitlab.com.com/my-group1/my-group2/my-project",
			},
			want: &want{
				projectPath: "my-group1/my-group2/my-project",
				objectPath:  "README.md",
				gitRef:      "main",
				objectType:  gitlab.ObjectTypeBlob,
				url:         "https://gitlab.com.com/my-group1/my-group2/my-project/-/blob/main/README.md",
			},
		},
		{
			description: "should parse absolute URL to project root with .git extension",
			args: &args{
				url: "https://gitlab.com.com/my-group1/my-group2/my-project.git",
			},
			want: &want{
				projectPath: "my-group1/my-group2/my-project",
				objectPath:  "README.md",
				gitRef:      "main",
				objectType:  gitlab.ObjectTypeBlob,
				url:         "https://gitlab.com.com/my-group1/my-group2/my-project/-/blob/main/README.md",
			},
		},
		{
			description: "should parse absolute URL to file",
			args: &args{
				url: "https://gitlab.com.com/my-group1/my-group2/my-project/-/blob/my-branch/my-file.txt",
			},
			want: &want{
				projectPath: "my-group1/my-group2/my-project",
				objectPath:  "my-file.txt",
				gitRef:      "my-branch",
				objectType:  gitlab.ObjectTypeBlob,
				url:         "https://gitlab.com.com/my-group1/my-group2/my-project/-/blob/my-branch/my-file.txt",
			},
		},
		{
			description: "should parse relative path to file",
			args: &args{
				url: "path/to/my-file.txt",
			},
			want: &want{
				projectPath: "",
				objectPath:  "path/to/my-file.txt",
				gitRef:      "",
				objectType:  gitlab.ObjectTypeBlob,
				url:         "path/to/my-file.txt",
			},
		},
		{
			description: "should parse URL with tree paths",
			args: &args{
				url: "https://gitlab.com/my-group/my-project/-/tree/my-branch",
			},
			want: &want{
				projectPath: "my-group/my-project",
				objectPath:  "",
				gitRef:      "my-branch",
				objectType:  gitlab.ObjectTypeTree,
				url:         "https://gitlab.com/my-group/my-project/-/tree/my-branch",
			},
		},
		{
			description: "should parse URL with issues path",
			args: &args{
				url: "https://gitlab.com/my-group/my-project/-/issues/123456",
			},
			want: &want{
				projectPath: "my-group/my-project",
				objectPath:  "123456",
				gitRef:      "",
				objectType:  gitlab.ObjectTypeIssues,
				url:         "https://gitlab.com/my-group/my-project/-/issues/123456",
			},
		},
	}

	assertProperty := func(t *testing.T, propertyName string, got string, want string) {
		if got != want {
			t.Errorf("%v: got %v, want %v", propertyName, got, want)
		}
	}

	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			u, err := gitlab.ParseURL("README.md")
			if err != nil {
				t.Fatal(err)
			}

			got, err := u.Parse(tc.args.url)
			if (err != nil) != tc.wantErr {
				t.Fatalf("unexpected error: %v", err)
			}
			assertProperty(t, "ProjectPath", got.ProjectPath(), tc.want.projectPath)
			assertProperty(t, "ObjectPath", got.ObjectPath(), tc.want.objectPath)
			assertProperty(t, "GitRef", got.GitRef(), tc.want.gitRef)
			assertProperty(t, "Type", string(got.ObjectType()), string(tc.want.objectType))

			if got, want := got.String(), tc.want.url; got != want {
				t.Fatalf("got %v, want %v", got, want)
			}
		})
	}
}

func TestURL_Origin(t *testing.T) {
	testCases := []struct {
		arg  string
		want string
	}{
		{
			arg:  "https://my-user:my-password@example.com/some/path",
			want: "https://my-user:my-password@example.com",
		},
		{
			arg:  "https://my-user:my-password@example.com:1234/some/path",
			want: "https://my-user:my-password@example.com:1234",
		},
		{
			arg:  "https://my-user:my-password@subdomain.example.com:1234/some/path",
			want: "https://my-user:my-password@subdomain.example.com:1234",
		},
		{
			arg:  "https://example.com/some/path",
			want: "https://example.com",
		},
		{
			arg:  "https://example.com:1234/some/path",
			want: "https://example.com:1234",
		},
	}

	for _, tc := range testCases {
		t.Run("should return expected origin", func(t *testing.T) {
			u, err := gitlab.ParseURL(tc.arg)
			if err != nil {
				t.Fatalf("unexpected error: %v", err)
			}
			got := u.Origin()
			if got != tc.want {
				t.Fatalf("got %q, want %q", got, tc.want)
			}
		})
	}
}
