package gitlab

import (
	"net/url"
	"path"
	"strings"
)

type ObjectType string

const (
	ObjectTypeBlob   ObjectType = "blob"
	ObjectTypeTree   ObjectType = "tree"
	ObjectTypeIssues ObjectType = "issues"
)

// URL defines the location of a Git/GitLab file.
type URL struct {
	url         *url.URL
	entrypoint  *URL
	projectPath string
	objectType  ObjectType
	objectPath  string
	gitRef      string
}

// ParseURL returns a URL for the file at the given location. The location is
// expected to be a local path or the URL as seen in the web browser when
// navigating to the file in GitLab.
func ParseURL(location string) (*URL, error) {
	u, err := url.Parse(location)
	if err != nil {
		return nil, err
	}
	return newURL(u, nil)
}

func ParseBlobURL(remoteURL string, gitRef string, filename string) (*URL, error) {
	ext := path.Ext(remoteURL)
	remoteURL = strings.TrimSuffix(remoteURL, ext)

	fileURL, err := url.JoinPath(remoteURL, "-", string(ObjectTypeBlob), gitRef, filename)
	if err != nil {
		return nil, err
	}
	return ParseURL(fileURL)
}

// IsRemote returns true if the URL is not located in the same project
// and branch/tag as the entrypoint URL.
func (u *URL) IsRemote() bool {
	return !(u.ProjectPath() == u.entrypoint.ProjectPath() &&
		u.GitRef() == u.entrypoint.GitRef())
}

// IsAbs reports whether the URL is absolute. Absolute means that it has a
// non-empty scheme.
func (u *URL) IsAbs() bool {
	return u.url.IsAbs()
}

// ProjectPath returns the path to the project root.
func (u *URL) ProjectPath() string {
	return u.projectPath
}

// GitRef returns the Git reference (i.e. branch, tag, commit hash).
func (u *URL) GitRef() string {
	return u.gitRef
}

// ObjectType returns the type of the object.
func (u *URL) ObjectType() ObjectType {
	return u.objectType
}

// ObjectPath returns the path of the object within the project.
func (u *URL) ObjectPath() string {
	return u.objectPath
}

// Host returns the host or host:port.
func (u *URL) Host() string {
	return u.url.Host
}

// Scheme returns the scheme of the URL.
func (u *URL) Scheme() string {
	return u.url.Scheme
}

// Path returns the full path of the URL.
func (u *URL) Path() string {
	return u.url.Path
}

// String returns the URL as a string.
func (u *URL) String() string {
	if u.IsRemote() {
		return u.url.String()
	}
	return u.ObjectPath()
}

// Parse parses the ref URL in the context of the receiver.
func (u *URL) Parse(ref string) (*URL, error) {
	refURL, err := u.url.Parse(ref)
	if err != nil {
		return nil, err
	}
	return newURL(refURL, u.entrypoint)
}

// Origin returns <scheme>://<user-info>@<hostname>:<port>.
// https://developer.mozilla.org/en-US/docs/Glossary/Origin
func (u *URL) Origin() string {
	origin := u.url.Scheme + "://"
	if u.url.User != nil {
		origin += u.url.User.String() + "@"
	}
	origin += u.Host()
	return origin
}

// NewURL parses the given URL as a GitLab URL. If nil is provided as the
// entrypoint, then the returned URL is marked as the entrypoint.
func newURL(u *url.URL, entrypoint *URL) (*URL, error) {
	projectPath := ""
	objectPath := ""
	gitRef := ""
	objectType := ObjectTypeBlob

	u.Path = strings.TrimPrefix(u.Path, "/")
	if u.IsAbs() {
		paths := strings.Split(u.Path, "/-/")
		projectPath = paths[0]
		isProjectRoot := len(paths) == 1
		if isProjectRoot {
			ext := path.Ext(projectPath)
			projectPath = strings.TrimSuffix(projectPath, ext)
			// Use README.md as the default file path when the URL points to the
			// project root.
			objectPath = "README.md"
			// Use "main" as the default branch.
			//
			// TODO: Investigate if we can get the
			// default branch from GitLab.
			gitRef = "main"
			p, err := url.JoinPath(projectPath, "-", string(ObjectTypeBlob), gitRef, objectPath)
			if err != nil {
				return nil, err
			}
			u.Path = p
		} else {
			// For the following, recall that file URLs are of the form
			// "blob/main/path/to/file.ext":
			filePathSegments := strings.Split(paths[1], "/")
			objectType = ObjectType(filePathSegments[0])

			if objectType == ObjectTypeBlob || objectType == ObjectTypeTree {
				if len(filePathSegments) > 1 {
					gitRef = filePathSegments[1]
				}
				if len(filePathSegments) > 2 {
					objectPath = path.Join(filePathSegments[2:]...)
				}
			} else {
				if len(filePathSegments) > 1 {
					objectPath = path.Join(filePathSegments[1:]...)
				}
			}
		}
	} else {
		objectPath = u.Path
	}

	uu := &URL{
		url:         u,
		entrypoint:  entrypoint,
		projectPath: projectPath,
		gitRef:      gitRef,
		objectType:  objectType,
		objectPath:  objectPath,
	}

	if uu.entrypoint == nil {
		uu.entrypoint = uu
	}

	return uu, nil
}
