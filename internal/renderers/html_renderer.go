// Package renderers provides the ability to render raw GitLab files (e.g.
// Markdown).
package renderers

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/samherrmann/glmd/internal/configuration"
	"gitlab.com/samherrmann/glmd/internal/gitlab"

	"gitlab.com/samherrmann/glmd/internal/mermaid"
	"gitlab.com/samherrmann/glmd/internal/stylesheets"
	"golang.org/x/net/html"
)

const htmlExt = ".html"

// HTMLRenderer converts files to HTML.
type HTMLRenderer struct {
	config *configuration.Config
	// fileLog tracks which files have already been rendered to prevent redundant
	// and infinite-loop rendering.
	fileLog      map[string]*gitlab.FileInfo
	gitlabClient *gitlab.Client
	gitlabURL    *gitlab.URL
	logger       *slog.Logger
}

// NewHTMLRenderer returns a new HTML renderer.
func NewHTMLRenderer(config *configuration.Config, logger *slog.Logger) (*HTMLRenderer, error) {
	gitlabURL, err := gitlab.ParseURL(config.GitLab.ProjectURL)
	if err != nil {
		return nil, err
	}
	gitlabClient := gitlab.NewClient(gitlabURL.Origin(), config.GitLab.Token)

	return &HTMLRenderer{
		config:       config,
		fileLog:      make(map[string]*gitlab.FileInfo),
		gitlabClient: gitlabClient,
		gitlabURL:    gitlabURL,
		logger:       logger,
	}, nil
}

// WriteTree writes the HTML converted file of filename and all its internally
// linked files to the renderer's output directory.
func (r *HTMLRenderer) WriteTree() error {

	file, err := gitlab.Stat(r.config.GitLab.ProjectURL, r.config.GitRef, r.config.Filename)
	if err != nil {
		return err
	}

	contents, err := os.ReadFile(r.config.Filename)
	if err != nil {
		return err
	}

	err = r.writeTree(file, string(contents), file)
	r.resetFileLog()
	return err
}

// writeTree converts the given contents to HTML and writes it to the file's
// destination within the renderer's output directory. writeTree is called
// recursively for all internal hyperlinks. anchorFile is the file that contains
// the hyperlink to file, and is used to resolve relative filenames. For the
// entrypoint file, anchorFile needs to be set to file. The contents are wrapped
// in a code block if the filename does not have a .md extension.
func (r *HTMLRenderer) writeTree(file *gitlab.FileInfo, contents string, anchorFile *gitlab.FileInfo) error {

	r.logger.Info(fmt.Sprintf("rendering %v", file.Dest()))

	fileExt := filepath.Ext(file.Dest())
	if fileExt != ".md" {
		language := strings.TrimPrefix(fileExt, ".")
		contents = markdownCodeBlock(contents, language)
		contents = fmt.Sprintf("# Source Code: %v\n%v", file.Dest(), contents)
	}

	htmlStr, err := r.gitlabClient.MarkdownToHTML(contents)
	if err != nil {
		return fmt.Errorf("cannot convert Markdown to HTML: %w:\n%v", err, contents)
	}

	htmlStr, err = r.addNavbar(file.Dest(), htmlStr)
	if err != nil {
		return fmt.Errorf("cannot attach navbar: %w", err)
	}

	documentNode, err := html.Parse(strings.NewReader(htmlStr))
	if err != nil {
		return fmt.Errorf("cannot parse HTML: %w", err)
	}

	htmlNode := documentNode.FirstChild
	headNode := htmlNode.FirstChild
	bodyNode := htmlNode.LastChild
	mainNode := bodyNode.LastChild

	// Insert stylesheets
	styleNodes, err := stylesheets.NewHTMLNodes(file.Dest())
	if err != nil {
		return err
	}
	for _, node := range styleNodes {
		headNode.AppendChild(node)
	}

	// Insert mermaid.bundle.js
	mermaidNode, err := mermaid.NewHTMLNode(file.Dest())
	if err != nil {
		return fmt.Errorf("creating Mermaid.js HTML node: %w", err)
	}
	bodyNode.AppendChild(mermaidNode)

	// Scan anchor nodes.
	anchorNodes := findNodes(mainNode, "a")
	gitlabHrefs, err := r.findGitLabBlobHyperlinks(anchorNodes)
	if err != nil {
		return fmt.Errorf("cannot find internal hyperlinks: %w", err)
	}

	returnError := false

	for _, href := range gitlabHrefs {
		file, err := anchorFile.Stat(href.Val)
		if err != nil {
			r.logger.Error(err.Error())
			returnError = true
			continue
		}

		relPath, err := file.RelDestPath(anchorFile)
		if err != nil {
			r.logger.Error(err.Error())
			returnError = true
			continue
		}
		href.Val = relPath + htmlExt
		src := file.Src().String()

		// Skip if the file has already been processed from a previous link.
		if r.fileLog[src] != nil {
			continue
		}
		r.fileLog[src] = file

		contents, err := file.Contents(r.config.GitLab.Token)
		if err != nil {
			// Failing to get the the contents of a file results in the documentation
			// having a broken link. Log the error but continue. A broken link should
			// not cause the documentation generation to stop.
			r.logger.Error(err.Error())
			returnError = true
			continue
		}
		var buf bytes.Buffer
		defer contents.Close()
		if _, err := io.Copy(&buf, contents); err != nil {
			return err
		}
		if err := r.writeTree(file, buf.String(), file); err != nil {
			returnError = true
			continue
		}
	}

	if err := r.writeFile(filepath.Join(r.config.OutputDir, file.Dest())+htmlExt, documentNode); err != nil {
		return fmt.Errorf("cannot write HTML to file: %w", err)
	}

	if returnError {
		return errors.New("broken link(s), see previous errors")
	}
	return nil
}

// resetFileLog re-initializes the file log to an empty map.
func (r *HTMLRenderer) resetFileLog() {
	r.fileLog = make(map[string]*gitlab.FileInfo)
}

// findGitLabBlobHyperlinks returns all HTML "href" attributes that link to other
// files in GitLab.
func (r *HTMLRenderer) findGitLabBlobHyperlinks(anchors []html.Node) ([]*html.Attribute, error) {
	attrs := []*html.Attribute{}
	for _, anchor := range anchors {
		for i, attr := range anchor.Attr {
			if attr.Key == "href" {
				u, err := gitlab.ParseURL(attr.Val)
				if err != nil {
					return nil, fmt.Errorf("cannot parse URL: %w", err)
				}
				if u.ObjectType() != gitlab.ObjectTypeBlob {
					continue
				}
				// Include relative paths but ignore links to sections within same
				// document:
				isIntraProjectPath := !u.IsAbs() && u.Path() != ""
				isGitLabURL := u.Host() == r.gitlabURL.Host()
				if isIntraProjectPath || isGitLabURL {
					attrs = append(attrs, &anchor.Attr[i])
				}
			}
		}
	}
	return attrs, nil
}

// writeFile writes the HTML node to the given filename.
func (r *HTMLRenderer) writeFile(filename string, node *html.Node) error {
	buf := &bytes.Buffer{}
	if err := html.Render(buf, node); err != nil {
		return err
	}
	if err := os.MkdirAll(filepath.Dir(filename), 0770); err != nil {
		return err
	}
	return os.WriteFile(filename, buf.Bytes(), 0666)
}

// findNodes returns all nodes that match the given HTML tag name.
func findNodes(root *html.Node, tag string) []html.Node {
	nodes := []html.Node{}

	var traverse func(node *html.Node, tag string)
	traverse = func(node *html.Node, tag string) {
		if node.Data == tag {
			nodes = append(nodes, *node)
		}
		if node.NextSibling != nil {
			traverse(node.NextSibling, tag)
		}
		if node.FirstChild != nil {
			traverse(node.FirstChild, tag)
		}
	}
	traverse(root, tag)
	return nodes
}

// addNavbar adds a navigation bar to the given HTML content for the file
// identified by filename. The content is wrapped in a <main> element.
func (r *HTMLRenderer) addNavbar(filename string, content string) (string, error) {

	relPath, err := filepath.Rel(filepath.Dir(filename), r.config.Filename)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf(
		`<nav><a href="%v">Home</a></nav><main>%v</main>`,
		relPath+htmlExt,
		content,
	), nil
}

func markdownCodeBlock(contents string, language string) string {
	codeDeclaration := "```"
	return fmt.Sprintf(
		"%v%v\n%v\n%v",
		codeDeclaration,
		language,
		contents,
		codeDeclaration,
	)
}
