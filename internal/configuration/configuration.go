// Package configuration is responsible for loading the application
// configuration.
package configuration

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"

	"github.com/samherrmann/flagerr"
	"gitlab.com/samherrmann/glmd/internal/git"
)

// ErrVersion is returned by Load when the -version flag is invoked.
var ErrVersion = errors.New("configuration: version request")

// Version is the version of the app. The value is set by the -ldflags flag of
// the go build command.
var Version = "dev"

// Config is the app configuration.
type Config struct {
	// Filename is the path to the entrypoint Markdown file, relative to the
	// project root.
	Filename string
	// ProjectRoot is the path to the root of the project.
	ProjectRoot string
	// GitRef is the current Git reference.
	GitRef string
	// OutputDir is the path to the output directory.
	OutputDir string
	// GitLab is contains the configuration to access the GitLab server
	GitLab *GitLabConfig
}

// GitLabConfig is the configuration to access the GitLab server.
type GitLabConfig struct {
	// ProjectURL of the remote repository on GitLab.
	ProjectURL string
	// Token is the GitLab access token.
	Token string
}

// Load loads and returns the app configuration. Note that this function changes
// the working directory of the app to the root directory of the Git working
// tree that the entrypoint file is located in.
func Load() (*Config, error) {
	flagSet := flagerr.NewFlagSet(os.Args[0])

	filename := flagSet.String("filename", "", "Path to the entrypoint Markdown file.")
	outputDir := flagSet.String("out", "dist/docs", "Path to the output directory.")
	gitlabToken := flagSet.String("token", "", "GitLab access token")
	versionRequested := flagSet.Bool("version", false, "Print version information and exit")
	flagSet.StringVar(filename, "f", *filename, "Alias for \"filename\" flag.")
	flagSet.BoolVar(versionRequested, "v", *versionRequested, "Alias for \"version\" flag.")

	if err := flagSet.Parse(os.Args[1:]); err != nil {
		return nil, err
	}

	if *versionRequested {
		fmt.Println(Version)
		return nil, ErrVersion
	}

	if *filename == "" {
		return nil, flagSet.UsageError("filename cannot be empty")
	}

	remoteURL, err := git.RemoteURL("origin", filepath.Dir(*filename))
	if err != nil {
		return nil, err
	}

	projectRoot, err := git.WorkingTreeRoot(filepath.Dir(*filename))
	if err != nil {
		return nil, err
	}

	gitRef, err := git.CurrentBranch(filepath.Dir(*filename))
	if err != nil {
		return nil, err
	}

	absFilename, err := filepath.Abs(*filename)
	if err != nil {
		return nil, err
	}

	// Path of entrypoint file relative to project root.
	relFilename, err := filepath.Rel(projectRoot, absFilename)
	if err != nil {
		return nil, err
	}

	absOutputDir, err := filepath.Abs(*outputDir)
	if err != nil {
		return nil, err
	}

	if err := os.Chdir(projectRoot); err != nil {
		return nil, err
	}

	return &Config{
		Filename:    relFilename,
		ProjectRoot: projectRoot,
		GitRef:      gitRef,
		OutputDir:   absOutputDir,
		GitLab: &GitLabConfig{
			ProjectURL: remoteURL.String(),
			Token:      *gitlabToken,
		},
	}, nil
}
