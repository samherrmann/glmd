// Package git provides a limited set of Go bindings for Git.
package git

import (
	"fmt"
	"net/url"
	"os/exec"
	"strings"
)

// RemoteURL returns the URL of the named remote repository for the local
// repository located in given path.
func RemoteURL(name string, path string) (*url.URL, error) {
	cmd := exec.Command("git", "remote", "get-url", name)
	cmd.Dir = path

	out, err := cmd.CombinedOutput()
	if err != nil {
		return nil, fmt.Errorf("git: cannot get URL of remote repository named %q: %w", name, err)
	}

	remoteURL, err := url.Parse(strings.TrimSuffix(string(out), "\n"))
	if err != nil {
		return nil, fmt.Errorf("git: cannot parse URL of remote repository named %q: %w", name, err)
	}

	return remoteURL, nil
}

// WorkingTreeRoot returns the absolute path of the top-level directory of the
// working tree.
func WorkingTreeRoot(path string) (string, error) {
	cmd := exec.Command("git", "rev-parse", "--show-toplevel")
	cmd.Dir = path

	out, err := cmd.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("git: cannot get working tree root path for %q: %w", path, err)
	}

	return strings.TrimSuffix(string(out), "\n"), nil
}

// CurrentBranch returns the currently checked-out branch of the repository at
// the given path.
func CurrentBranch(path string) (string, error) {
	cmd := exec.Command("git", "rev-parse", "--abbrev-ref", "HEAD")
	cmd.Dir = path

	out, err := cmd.CombinedOutput()
	if err != nil {
		return "", fmt.Errorf("git: cannot get current branch of repository in path %q: %w", path, err)
	}

	return strings.TrimSuffix(string(out), "\n"), nil
}
