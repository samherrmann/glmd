package git

import (
	"os"
	"path"
	"path/filepath"
	"testing"
)

func TestRemoteURL(t *testing.T) {
	type args struct {
		path string
	}
	tests := []struct {
		description string
		args        args
		want        string
	}{
		{
			description: "should return the URL of the remote repository",
			args:        args{path: ""},
			want:        "https://gitlab.com/samherrmann/glmd.git",
		},
	}
	for _, tc := range tests {
		t.Run(tc.description, func(t *testing.T) {
			u, err := RemoteURL("origin", tc.args.path)
			if err != nil {
				t.Errorf("unexpected error: %v", err)
				return
			}
			got := u.Scheme + "://" + path.Join(u.Host, u.Path)
			if got != tc.want {
				t.Errorf("RemoteURL() = %v, want %v", got, tc.want)
			}
		})
	}
}

func TestWorkingTreeRoot(t *testing.T) {

	t.Run("should return working tree root path", func(t *testing.T) {
		wd, err := os.Getwd()
		if err != err {
			t.Fatalf("unexpected error: %v", err)
		}
		got, err := WorkingTreeRoot(wd)
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
			return
		}
		want := filepath.Join(wd, "../..")
		if got != want {
			t.Errorf("got: %v, want %v", got, want)
		}
	})
}
