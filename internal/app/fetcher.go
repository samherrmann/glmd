package app

import (
	"fmt"
	"io"
	"io/fs"
	"net/url"
	"path/filepath"
	"strings"

	"gitlab.com/samherrmann/glmd/internal/gitlab"
	"gitlab.com/samherrmann/glmd/pkg/crawler"
)

func NewFetcher(gitLabClient *gitlab.Client, fs fs.FS) *Fetcher {
	return &Fetcher{
		glServerAddr: gitLabClient.ServerURL(),
		mdConverter:  gitLabClient,
		glFetcher:    gitlab.NewFetcher(gitLabClient),
		fsFetcher:    crawler.NewFSFetcher(fs),
	}
}

type Fetcher struct {
	// Replace allows a prefix of an address that is passed to Fetch to be
	// replaced with an alternate prefix. This provides the ability to selectively
	// redirect URLs to the local file system.
	Replace      map[string]string
	glServerAddr string
	mdConverter  MarkdownConverter
	glFetcher    crawler.Fetcher
	fsFetcher    crawler.Fetcher
}

// Fetch implements the [crawler.Fetcher] interface. If the prefix of address is
// in the Replace map, then Fetch fetches the resource from the file system at
// the replaced path. Otherwise, if address is a GitLab address, then Fetch
// fetches the address from GitLab. Fetch returns an error for any other
// address.
func (f *Fetcher) Fetch(address string) (io.ReadCloser, error) {
	// Check if prefix of address is in the replace map.
	for addrPrefix, replacePath := range f.Replace {
		if strings.HasPrefix(address, addrPrefix) {
			filename := strings.Replace(address, addrPrefix, replacePath, 1)
			return f.fetchFS(filepath.Clean(filename))
		}
	}

	// Check if address is a GitLab address.
	reqURL, err := url.ParseRequestURI(address)
	if err != nil {
		return nil, err
	}
	reqOrigin := fmt.Sprintf("%v://%v", reqURL.Scheme, reqURL.Host)
	if reqOrigin != f.glServerAddr {
		return nil, fmt.Errorf("address not supported: %v", address)
	}

	return f.glFetcher.Fetch(address)
}

func (f *Fetcher) fetchFS(filename string) (io.ReadCloser, error) {
	resp, err := f.fsFetcher.Fetch(filename)
	if err != nil {
		return nil, err
	}
	defer resp.Close()

	buf, err := io.ReadAll(resp)
	if err != nil {
		return nil, fmt.Errorf("reading file contents: %w", err)
	}

	doc, err := f.mdConverter.MarkdownToHTML(string(buf))
	if err != nil {
		return nil, fmt.Errorf("converting markdown to html: %w", err)
	}

	return io.NopCloser(strings.NewReader(doc)), nil
}

type MarkdownConverter interface {
	MarkdownToHTML(md string) (string, error)
}
