package app

import (
	"io"
	"strings"
	"testing"
)

func TestFetcher_Fetch(t *testing.T) {
	tests := []struct {
		name         string
		replace      map[string]string
		glServerAddr string
		address      string
		want         string
		wantErr      bool
	}{
		{
			name:         "supported address",
			glServerAddr: "https://foo",
			address:      "https://foo/a/b/c",
			want:         "https://foo/a/b/c",
		},
		{
			name:         "unsupported address",
			glServerAddr: "https://foo",
			address:      "https://bar/a/b/c",
			wantErr:      true,
		},
		{
			name:    "redirect to file system",
			address: "https://bar/a/b/c",
			replace: map[string]string{
				"https://bar": "/path/to/folder",
			},
			want: "/path/to/folder/a/b/c",
		},
		{
			name:    "redirect to file system with trailing slash",
			address: "https://bar/a/b/c",
			replace: map[string]string{
				"https://bar": "/path/to/folder/",
			},
			want: "/path/to/folder/a/b/c",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &Fetcher{
				Replace:      tt.replace,
				glServerAddr: tt.glServerAddr,
				glFetcher:    &stubFetcher{},
				fsFetcher:    &stubFetcher{},
				mdConverter:  &mdConverterStub{},
			}

			resp, err := f.Fetch(tt.address)
			if (err != nil) != tt.wantErr {
				t.Errorf("error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil {
				return
			}
			defer resp.Close()

			b, err := io.ReadAll(resp)
			if err != nil {
				t.Fatal(err)
			}

			got := string(b)
			if got != tt.want {
				t.Fatalf("got %v, want %v", got, tt.want)
			}
		})
	}
}

type stubFetcher struct{}

func (*stubFetcher) Fetch(address string) (io.ReadCloser, error) {
	return io.NopCloser(strings.NewReader(address)), nil
}

type mdConverterStub struct{}

func (*mdConverterStub) MarkdownToHTML(md string) (string, error) {
	return md, nil
}
