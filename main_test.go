//go:build e2e

package main

import (
	"bytes"
	"io/fs"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"testing"
	"text/template"

	"gitlab.com/samherrmann/glmd/internal/git"
	"golang.org/x/net/html"
)

const outDir = "out/docs"

func TestApp(t *testing.T) {
	type filename struct {
		GitRef string
	}
	// evalFilename returns a filename that is the result of the given text
	// template and the current Git reference.
	evalFilename := func(text string) (string, error) {
		gitRef, err := git.CurrentBranch(".")
		if err != nil {
			return "", err
		}
		tpl, err := template.New("filename").Parse(text)
		if err != nil {
			return "", err
		}
		result := &bytes.Buffer{}
		if err := tpl.Execute(result, &filename{GitRef: gitRef}); err != nil {
			return "", err
		}
		return result.String(), nil
	}

	testCases := []struct {
		description string
		filename    string
		want        string
	}{
		{
			description: "should render entrypoint file",
			filename:    "README.md.html",
			want:        "glmd",
		}, {
			description: "should render file linked by absolute URL",
			filename:    "samherrmann/glmd/main/testdata/absolute-url-link.md.html",
			want:        "Absolute URL Link",
		}, {
			description: "should render file linked by relative path",
			filename:    "samherrmann/glmd/{{ .GitRef }}/testdata/relative-path-link.md.html",
			want:        "Relative Path Link",
		},
	}

	clean := func() {
		if err := os.RemoveAll(outDir); err != nil {
			t.Fatal(err)
		}
	}

	// Make sure the output directory is clean before running the tests and also
	// clean up afterwards.
	clean()
	defer clean()

	cmd := exec.Command("go", "run", ".", "-f", "README.md", "-out", outDir, "-token", os.Getenv("GITLAB_ACCESS_TOKEN"))
	out, err := cmd.CombinedOutput()
	if err != nil {
		t.Fatalf("%v: %s", err, out)
	}

	for _, tc := range testCases {
		t.Run(tc.description, func(t *testing.T) {
			filename, err := evalFilename(tc.filename)
			if err != nil {
				t.Fatal(err)
			}
			contents, err := os.ReadFile(filepath.Join(outDir, filename))
			if os.IsNotExist(err) {
				t.Error(err)
				filepath.WalkDir(outDir, func(path string, d fs.DirEntry, err error) error {
					t.Log(path)
					return err
				})
				return
			}
			if err != nil {
				t.Fatal(err)
			}
			got := string(contents)
			// Verify the contents are valid HTML.
			if _, err := html.Parse(strings.NewReader(got)); err != nil {
				t.Fatalf("Failed to parse HTML: %q", contents)
			}
			// verify the contents.
			if !strings.Contains(got, tc.want) {
				t.Fatalf("Rendered HTML does not contain %q, got: %q", tc.want, contents)
			}
		})
	}
}
