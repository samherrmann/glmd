SHELL := /bin/bash
MAKEFLAGS	+= --silent

# Tags that may be passed in as arguments.
tags := "e2e"
# Build target operating system.
# https://golang.org/doc/install/source#environment
goos := $(shell go env GOOS)
# Build target architecture.
# https://golang.org/doc/install/source#environment
app_name := glmd
goarch := $(shell go env GOARCH)
platform := $(goos)-$(goarch)
variant := $(app_name)-$(platform)
dist_path := dist
bin_path := $(dist_path)/$(variant)
version := $(shell git describe --dirty --tags --always)
package_registry_url := "$(CI_API_V4_URL)/projects/$(CI_PROJECT_ID)/packages/generic/$(app_name)"
# TODO: Mermaid v10 only supports ESM but ESMs cannot be loaded in browsers via
# the file:// protocol due to CORS policy. Investigae how we can use the latest
# version of Mermaid without requiring a web server to view the output.
# - https://github.com/mermaid-js/mermaid/releases/tag/v10.0.0
# - https://github.com/whatwg/html/issues/8121
mermaid_url := https://cdn.jsdelivr.net/npm/mermaid@9/dist/mermaid.min.js

run: mermaid
	go run . -f README.md -out out/docs -token=$(GITLAB_ACCESS_TOKEN)

lint: mermaid
	staticcheck -checks=all ./...

test: mermaid
	go test -cover -race -tags "$(tags)" ./...

test-skip-e2e:
	make test tags=""

build: mermaid
	mkdir -p $(dist_path)
# See linker flags here: https://pkg.go.dev/cmd/link
	go build -ldflags "-w -s -X 'gitlab.com/samherrmann/glmd/configuration.Version=$(version)'" -o $(bin_path) .

publish:
	curl --header "JOB-TOKEN: $(CI_JOB_TOKEN)" --upload-file $(bin_path) $(package_registry_url)/$(CI_COMMIT_TAG)/$(archive)

clean:
	git clean -fdX

mermaid:
ifeq (,$(wildcard internal/mermaid/mermaid.bundle.js))
	npm run build
endif
