module gitlab.com/samherrmann/glmd

go 1.22.2

toolchain go1.22.3

require golang.org/x/net v0.0.0-20220513224357-95641704303c

require github.com/samherrmann/flagerr v0.0.0-20240614210623-8fa00fb14972
