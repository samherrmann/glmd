# Examples

* [Absolute URL link](https://gitlab.com/samherrmann/glmd/-/blob/main/testdata/absolute-url-link.md)
* [Relative path link](relative-path-link.md)
* [Code spans and blocks](code.md)
* [Code file](../main.go)
* [Mermaid](mermaid.md)
