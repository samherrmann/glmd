package main

import (
	"fmt"
	"log/slog"
	"os"

	"github.com/samherrmann/flagerr"
	"gitlab.com/samherrmann/glmd/internal/configuration"
	"gitlab.com/samherrmann/glmd/internal/mermaid"
	"gitlab.com/samherrmann/glmd/internal/renderers"
	"gitlab.com/samherrmann/glmd/internal/stylesheets"
)

func main() {
	if err := app(); err != nil {
		logError(err)
	}
}

func app() error {
	// Load the app configuration.
	config, err := configuration.Load()
	if err == configuration.ErrVersion {
		return nil
	}
	if err != nil {
		return err
	}
	// Create a logger.
	logger := slog.New(slog.NewTextHandler(os.Stderr, nil))
	// Clean output directory:
	if err := os.RemoveAll(config.OutputDir); err != nil {
		return fmt.Errorf("cannot clean output directory: %w", err)
	}
	// Write embedded stylesheet to output directory:
	if err := stylesheets.WriteFile(config.OutputDir); err != nil {
		return err
	}
	// Write embedded mermaid script to output directory:
	if err := mermaid.WriteFile(config.OutputDir); err != nil {
		return err
	}
	// Render entrypoint file and the internal files it links to.
	r, err := renderers.NewHTMLRenderer(config, logger)
	if err != nil {
		return err
	}
	return r.WriteTree()
}

func logError(err error) {
	// If the -help or -h flag was invoked, then print the error to standard out
	// and exit normally (i.e. with code 0).
	if flagerr.IsHelpError(err) {
		fmt.Println(err)
		return
	}
	// Print all other errors to os.Stdout. and exit with code 1.
	fmt.Fprintf(os.Stderr, "Error: %v\n", err)
	os.Exit(1)
}
